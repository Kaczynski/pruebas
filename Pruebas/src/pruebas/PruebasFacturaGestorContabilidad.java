package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.Cliente;
import com.Factura;
import com.GestorContabilidad;

public class PruebasFacturaGestorContabilidad {

	static GestorContabilidad gc;
	static Logger log;
	static long startTime;
	static long endTime;
	static Cliente cliente;
	static Factura factura;
	static Factura facturaNull;

	@BeforeClass
	public static void antesDeNa() {
		gc = new GestorContabilidad();
		cliente = new Cliente("ClienteB", "752f55d", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(cliente);
		factura = new Factura("526253", LocalDate.parse("2018-05-11"), "ps2", 2.5f, 10, cliente);
		facturaNull = new Factura("526253", LocalDate.parse("2018-05-11"), "ps2", 2.5f, 10, null);
	}

	/**
	 * Nueva vactura en lista vacia
	 */
	@Before
	public void init() {
		gc.getListaFacturas().clear();
	}
	
	@Test
	public void testEmptyListcrearFactura() {
		gc.crearFactura(factura);
		boolean expected = gc.getListaFacturas().contains(factura);
		assertTrue(expected);
	}

	/**
	 * Nueva factura en lista con datos
	 */
	@Test
	public void testListWithRecordsCrearFactura() {
		gc.getListaFacturas().add(factura);
		Factura fa = new Factura("52652da5", LocalDate.parse("2006-05-11"), "pa2", 2f, 1, cliente);

		gc.crearFactura(fa);

		boolean expected = gc.getListaFacturas().contains(fa);
		assertTrue(expected);
	}

	/**
	 * alta de ua factura ya existente en la lista
	 */
	@Test
	public void testExistingCrearFactura() {
		gc.getListaFacturas().add(factura);
		gc.getListaFacturas().add(factura);
		Factura fa = new Factura("526253", LocalDate.parse("2006-05-11"), "pa2", 2f, 1, cliente);

		gc.crearFactura(fa);

		boolean expected = gc.getListaFacturas().contains(fa);
		assertFalse(expected);
	}

	/**
	 * Buscar una facura que existe
	 */
	@Test
	public void testExistingBuscarFactura() {
		gc.getListaFacturas().add(factura);

		Factura actual = gc.buscarFactura("526253");

		assertSame(factura, actual);
	}

	/**
	 * Buscar una Factura que no existe
	 */
	@Test
	public void testWrongBuscarFactura() {
		gc.getListaFacturas().add(factura);
		Factura actual = gc.buscarFactura("52625as3");

		assertNull(actual);
	}

	/**
	 * Buscar una Factura en lista vacia
	 * 
	 */
	@Test
	public void testEmptyLsitBuscarFactura() {
		Factura actual = gc.buscarFactura("52625as3");

		assertNull(actual);
	}

	/**
	 * Mostrar la factura mas cara de una lista vacia.
	 */
	@Test
	public void testFacturaMasCaraEmpty() {

		Factura actual = gc.facturaMasCara();

		assertNull(actual);
	}

	/**
	 * Mostrar la factura mas cara de una lista con un elemento.
	 */
	@Test
	public void testExistingFacturaMasCara() {
		gc.getListaFacturas().add(factura);

		Factura actual = gc.facturaMasCara();

		assertSame(factura, actual);
	}

	/**
	 * Mostrar la factura mas cara de una lista con varios elementos.
	 */
	@Test
	public void testWithRecordsFacturaMasCara() {
		gc.getListaFacturas().add(factura);
		Factura other = new Factura("526d253", LocalDate.parse("2065-05-11"), "pa2", 1.6f, 2, cliente);
		Factura another = new Factura("526dq253", LocalDate.parse("2085-05-11"), "pa2", 2.6f, 9, cliente);
		gc.getListaFacturas().add(other);
		gc.getListaFacturas().add(another);

		Factura actual = gc.facturaMasCara();

		assertSame(factura, actual);
	}

	/**
	 * Mostrar la factura mas cara de una lista con varios elementos con mismo
	 * precio.
	 */
	@Test
	public void testWithSameFacturaMasCara() {
		gc.getListaFacturas().add(factura);
		Factura other = new Factura("526d253", LocalDate.parse("2065-05-11"), "pa2", 1.6f, 2, cliente);
		Factura another = new Factura("526dq253", LocalDate.parse("2085-05-11"), "pa2", 2.6f, 5, cliente);
		gc.getListaFacturas().add(other);
		gc.getListaFacturas().add(another);

		Factura actual = gc.facturaMasCara();

		assertSame(factura, actual);
	}

	/**
	 * Calcuulo de factura sin registros
	 */
	@Test
	public void testEmptyCalcularFacturacionAnual() {
		float expected = 0f;

		float actual = gc.calcularFacturacionAnual(2005);

		assertEquals(expected, actual, 0);
	}

	/**
	 * Calcuulo de factura anual de a�o inexistente
	 */
	@Test
	public void testNonExistentCalcularFacturacionAnual() {
		gc.getListaFacturas().add(factura);
		float expected = 0f;

		float actual = gc.calcularFacturacionAnual(2005);

		assertEquals(expected, actual, 0);
	}

	/**
	 * Calcuulo de factura anual de un solo registro
	 */
	@Test
	public void testExistentCalcularFacturacionAnual() {
		gc.getListaFacturas().add(factura);
		Factura e = new Factura("526253", LocalDate.parse("2005-05-11"), "p2", 2.6f, 1, cliente);
		gc.getListaFacturas().add(e);
		float expected = 2.6f;

		float actual = gc.calcularFacturacionAnual(2005);

		assertEquals(expected, actual, 0);
	}

	/**
	 * Calcuulo de factura anual de varios registros
	 */
	@Test
	public void testMultiExistentCalcularFacturacionAnual() {
		gc.getListaFacturas().add(factura);
		Factura es = new Factura("526253", LocalDate.parse("2015-05-11"), "pa2", 1, 10, cliente);
		gc.getListaFacturas().add(es);
		gc.getListaFacturas().add(facturaNull);
		float expected = 50;

		float actual = gc.calcularFacturacionAnual(2018);
		assertEquals(expected, actual, 0.001);
	}

	/**
	 * Calcuulo de factura anual de varios registros
	 */
	@Test
	public void testEmptyEliminarFactura() {
		gc.getListaFacturas().add(factura);
		gc.eliminarFactura("");
		assertTrue(gc.getListaFacturas().contains(factura));
	}

	/**
	 * Calcuulo de factura anual de varios registros
	 */
	@Test
	public void tesEliminarFactura() {
		gc.getListaFacturas().add(factura);
		gc.eliminarFactura("526253");
		boolean result = gc.getListaFacturas().contains(factura);
		assertFalse(result);
	}

}
