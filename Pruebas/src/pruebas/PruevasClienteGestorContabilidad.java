package pruebas;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.Cliente;
import com.Factura;
import com.GestorContabilidad;

public class PruevasClienteGestorContabilidad {

	static GestorContabilidad gc;
	static Logger log;
	static long startTime;
	static long endTime;

	@BeforeClass
	public static void antesDeNa() {
		gc = new GestorContabilidad();
		log = Logger.getLogger(PruevasClienteGestorContabilidad.class.getName());
	}

	/**
	 * Nuevo cliete en lista vacia
	 */
	@Before
	public void init() {
		gc.getListaClientes().clear();
	}
	@Test
	public void testEmptyListAltaCliente() {
		
		Cliente clientea = new Cliente("ClienteA", "11684", LocalDate.parse("2018-02-02"));

		gc.altaCliente(clientea);

		boolean expected = gc.getListaClientes().contains(clientea);
		assertTrue(expected);
	}

	/**
	 * Nuevo cliente en lista con datos
	 */
	@Test
	public void testListWithRecordsAltaCliente() {
		
		Cliente ca = new Cliente("ClienteB", "752f55d", LocalDate.parse("2019-03-05"));
		Cliente da = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(ca);

		gc.altaCliente(da);

		boolean expected = gc.getListaClientes().contains(da);
		assertTrue(expected);
	}

	/**
	 * alta de un cliente ya existente en la lista
	 */
	@Test
	public void testExistingAltaCliente() {
		
		Cliente db = new Cliente("testAltaClienteExistenteB", "826865644", LocalDate.parse("2019-03-05"));
		Cliente cb = new Cliente("ClienteC", "826865644", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(cb);

		gc.altaCliente(db);

		boolean expected = gc.getListaClientes().contains(db);
		assertFalse(expected);
	}

	/**
	 * Buscar un dni que existe
	 */
	@Test
	public void testExistingBuscarCliente() {
		
		Cliente expected = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(expected);

		Cliente actual = gc.buscarCliente("5sdf55sd");

		assertSame(expected, actual);
	}

	/**
	 * Buscar un dni que no existe
	 */
	@Test
	public void testWrongBuscarCliente() {
		
		Cliente expected = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(expected);

		Cliente actual = gc.buscarCliente("00000000");

		assertNull(actual);
	}

	/**
	 * Buscar un cliente en lista vacia
	 * 
	 */
	@Test
	public void testEmptyLsitBuscarCliente() {
		

		Cliente actual = gc.buscarCliente("00000000");

		assertNull(actual);
	}

	/**
	 * Buscar cliente mas antiguo en lista vacia
	 */
	@Test
	public void testEmptyListClienteMasAntiguo() {
		Cliente actual = gc.clienteMasAntiguo();
		assertNull(actual);
	}

	/**
	 * Buscar cliente mas antiguo en lista fecha unica
	 */
	@Test
	public void testOneRecordClienteMasAntiguo() {
		
		Cliente expected = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(expected);

		Cliente actual = gc.clienteMasAntiguo();

		assertSame(expected, actual);
	}

	/**
	 * Buscar cliente mas antiguo en lista fecha unica
	 */
	@Test
	public void testWithRecordsClienteMasAntiguo() {
		
		Cliente expected = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("1999-03-05"));
		gc.getListaClientes().add(expected);
		Cliente cc = new Cliente("ClienfteC", "5sdf55asdsd", LocalDate.parse("2020-03-05"));
		gc.getListaClientes().add(cc);
		Cliente dd = new Cliente("ClienfteC", "5sdfsd", LocalDate.parse("1999-03-06"));
		gc.getListaClientes().add(dd);

		Cliente actual = gc.clienteMasAntiguo();

		assertSame(expected, actual);
	}

	/**
	 * Buscar cliente mas antiguo en lista fecha duplicada
	 */
	@Test
	public void testWithSameDateClienteMasAntiguo() {
		
		Cliente expected = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("1999-03-05"));
		gc.getListaClientes().add(expected);
		Cliente cc = new Cliente("ClienfteC", "5sdf55asdsd", LocalDate.parse("2020-03-05"));
		gc.getListaClientes().add(cc);
		Cliente dd = new Cliente("ClienfteC", "5sdf5d", LocalDate.parse("1999-03-05"));
		gc.getListaClientes().add(dd);

		Cliente actual = gc.clienteMasAntiguo();

		assertSame(expected, actual);
	}

	/**
	 * Eliminar cliente de una lista vacia
	 */
	@Test
	public void testEmptyListEliminarCliente() {
		

		try {
			gc.eliminarCliente("453253ads");
		} catch (Exception e) {
			fail("No exception was expected");
		}

	}

	/**
	 * Eliminar cliente inexistente de una lista
	 */
	@Test
	public void testWrongEliminarCliente() {
		
		Cliente cc = new Cliente("ClienfteC", "5sdf55asdsd", LocalDate.parse("2020-03-05"));
		gc.getListaClientes().add(cc);

		try {
			gc.eliminarCliente("");
		} catch (Exception e) {
			fail("No exception was expected");
		}

	}

	/**
	 * Eliminar cliente sin facturas
	 */
	@Test
	public void testEliminarCliente() {
		
		Cliente cc = new Cliente("ClienfteC", "5sdf55asdsd", LocalDate.parse("2020-03-05"));
		gc.getListaClientes().add(cc);

		gc.eliminarCliente("5sdf55asdsd");

		assertFalse(gc.getListaClientes().contains(cc));
	}

	/**
	 * Eliminar cliente con facturas
	 */
	@Test
	public void testFacturasEliminarCliente() {
		
		Cliente cc = new Cliente("ClienfteC", "5sdf55asdsds", LocalDate.parse("2020-03-05"));
		gc.getListaClientes().add(cc);
		gc.getListaFacturas().clear();
		Factura fc = new Factura("526253", LocalDate.parse("2005-05-11"), "p2", 2.6f, 3, cc);
		gc.getListaFacturas().add(fc);

		gc.eliminarCliente("5sdf55asdsds");

		assertTrue(gc.getListaClientes().contains(cc));
	}

}
