package pruebas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.Cliente;
import com.Factura;
import com.GestorContabilidad;

public class PruebasFacturaClienteGestorContabilidad {

	static GestorContabilidad gc;

	@BeforeClass
	public static void antesDeNa() {
		gc = new GestorContabilidad();

	}
	@After
	public void ini() {
		gc.getListaClientes().clear();
		gc.getListaFacturas().clear();
	}
	/**
	 * AsignarClienteAFactura cliente inexistente factura inexistente
	 */
	@Test
	public void testNonExistAsignarClienteAFactura() {


		try {
			gc.asignarClienteAFactura("", "");
		} catch (Exception e) {
			fail("no se esperaba una excepcion");
		}

	}

	/**
	 * AsignarClienteAFactura cliente a factura inexistente
	 */
	@Test
	public void testEmptyAsignarClienteAFactura() {
		Cliente da = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(da);

		try {
			gc.asignarClienteAFactura("5sdf55sd", "");
		} catch (Exception e) {
			fail("no se esperaba una excepcion");
		}

	}

	/**
	 * AsignarClienteAFactura cliente a factura
	 */
	@Test
	public void testAsignarClienteAFactura() {
		Cliente da = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(da);
		Factura e = new Factura("526253", LocalDate.parse("2006-05-11"), "ps2", 2.5f, 10, null);
		gc.getListaFacturas().add(e);

		gc.asignarClienteAFactura("5sdf55sd", "526253");

		boolean result = (gc.getListaFacturas().get(gc.getListaFacturas().indexOf(e)).getCliente() == null
				|| !gc.getListaFacturas().get(gc.getListaFacturas().indexOf(e)).getCliente().equals(da)) ? false : true;

		assertTrue(result);

	}

	/**
	 * CantidadFacturasPorCliente cliente
	 */
	@Test
	public void testCantidadFacturasPorCliente() {
		Cliente da = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(da);
		Factura e = new Factura("526253", LocalDate.parse("2006-05-11"), "ps2", 2.5f, 10, da);
		gc.getListaFacturas().add(e);
		Factura es = new Factura("52625a3", LocalDate.parse("2006-05-11"), "psa2", 2.5f, 10, da);
		gc.getListaFacturas().add(es);

		int result = gc.cantidadFacturasPorCliente("5sdf55sd");

		assertEquals(result, 2);
	}

	/**
	 * CantidadFacturasPorCliente cliente sin facturas
	 */
	@Test
	public void testEmptyCantidadFacturasPorCliente() {
		Cliente da = new Cliente("ClienteC", "5sdf55sd", LocalDate.parse("2019-03-05"));
		gc.getListaClientes().add(da);
		Factura e = new Factura("526253", LocalDate.parse("2006-05-11"), "ps2", 2.5f, 10, null);
		gc.getListaFacturas().add(e);
		Factura es = new Factura("52625a3", LocalDate.parse("2006-05-11"), "psa2", 2.5f, 10, null);
		gc.getListaFacturas().add(es);

		int result = gc.cantidadFacturasPorCliente("5sdf55sd");

		assertEquals(result, 0);
	}

	/**
	 * CantidadFacturasPorCliente cliente lista vacia
	 */
	@Test
	public void testEptyListClienteCantidadFacturasPorCliente() {
		int result = gc.cantidadFacturasPorCliente("5sdf55sd");

		assertEquals(result, 0);
	}

}
