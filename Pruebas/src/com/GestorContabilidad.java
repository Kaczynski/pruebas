package com;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas = new ArrayList<>();
	private ArrayList<Cliente> listaClientes = new ArrayList<>();

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	// *****************
	public Cliente buscarCliente(String dni) {
		for (Cliente c : listaClientes) {
			if (c.getDni().equalsIgnoreCase(dni)) {
				return c;
			}
		}
		return null;
	}

	public Factura buscarFactura(String codigo) {
		for (Factura f : listaFacturas) {
			if (f.getCodigoFactura().equalsIgnoreCase(codigo)) {
				return f;
			}
		}
		return null;
	}

	public void altaCliente(Cliente cliente) {
		boolean check = false;
		for (Cliente c : listaClientes) {
			if (c.getDni().equalsIgnoreCase(cliente.getDni())) {
				check = true;
			}
		}
		if (!check) {
			listaClientes.add(cliente);
		}
	}

	public void crearFactura(Factura factura) {
		boolean check = false;
		for (Factura f : listaFacturas) {
			if (f.getCodigoFactura().equalsIgnoreCase(factura.getCodigoFactura())) {
				check = true;
			}
		}
		if (!check) {
			listaFacturas.add(factura);
		}
	}

	public Cliente clienteMasAntiguo() {
		Cliente cliente = null;
		for (Cliente c : listaClientes) {
			cliente = (cliente == null || c.getFechaAlta().isBefore(cliente.getFechaAlta())) ? c : cliente;
		}

		return cliente;
	}

	public Factura facturaMasCara() {
		Factura fac = null;
		for (Factura f : listaFacturas) {
			fac = (fac == null || f.calcularPrecioTotal() > fac.calcularPrecioTotal()) ? f : fac;
		}
		return fac;
	}

	public float calcularFacturacionAnual(int anno) {
		float suma = 0;
		for (Factura f : listaFacturas) {
			if (f.getFecha().getYear() == anno) {
				suma += f.calcularPrecioTotal();
			}
		}
		return suma;
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		Factura fac=buscarFactura(codigoFactura);
		if(fac!=null) {
			Cliente cli = buscarCliente(dni);
			if(cli !=null ) {
				fac.setCliente(cli);
			}
		}
	}

	public int cantidadFacturasPorCliente(String dni) {
		int aux = 0;
		for (Factura f : listaFacturas) {
			if (f.getCliente()!=null && f.getCliente().getDni().equalsIgnoreCase(dni)) {
				aux++;
			}
		}
		return aux;
	}

	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}

	public void eliminarCliente(String dni) {

		if (cantidadFacturasPorCliente(dni) == 0) {
			listaClientes.remove(buscarCliente(dni));
		}

	}
}
