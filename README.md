# Practica 4.1 Diseño de pruebas unitarias

>Pruebas unitarias necesarias para comprobar el funcionamiento del programa. Para ello se utilizo la herramienta JUnit. 
>
>El programa consta de 3 clases, aunque todos los métodos se encuentran en una sola clase.
>
>Se ha tratado dentro de lo posible de plantear un enfoque de desarrollo guiado por pruebas (TDD), que consiste en diseñar las pruebas antes de implementar el código.

Autor: Lukasz Kaczynski